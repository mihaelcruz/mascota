package com.mihaelcruz.mascota

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_destino.*

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)

        val bundleRecepcion : Bundle? = intent.extras

        bundleRecepcion?.let { bundleNulo ->
            val nombre = bundleNulo.getString("key_nombre", "")
            val edad = bundleNulo.getString("key_edad","")
            val tipo = bundleNulo.getString("key_tipo","")
            val vacuna1 = bundleNulo.getString("key_vacuna1","")
            val vacuna2 = bundleNulo.getString("key_vacuna2","")
            val vacuna3 = bundleNulo.getString("key_vacuna3","")

            tvNombre.text = "Nombre: $nombre"
            tvEdad.text = "Edad: $edad"
            when(tipo){
                in "perro"->imgMascotaResultado.setImageResource(R.drawable.dog)
                in "gato"->imgMascotaResultado.setImageResource(R.drawable.cat)
                else -> imgMascotaResultado.setImageResource(R.drawable.bunny)
            }

            if(vacuna1=="check"){
                checkDistemperResultado.isChecked = true
            }
            if(vacuna2=="check"){
                checkParvovirusResultado.isChecked = true
            }
            if(vacuna3=="check"){
                checkHepatitisResultado.isChecked = true
            }


        }

    }
}