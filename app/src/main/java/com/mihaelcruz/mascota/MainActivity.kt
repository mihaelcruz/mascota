package com.mihaelcruz.mascota

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnEnviar.setOnClickListener{
            val nombre = edtNombre.text.toString()
            val edad = edtEdad.text.toString()
            if (nombre.isEmpty()){
                Toast.makeText(this, "Debe ingresar nombre", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (edad.isEmpty()){
                Toast.makeText(this, "Debe ingresar edad", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            var tipo = ""
            if(radioPerro.isChecked){
                tipo = "perro"
            }
            if(radioGato.isChecked){
                tipo = "gato"
            }
            if(radioConejo.isChecked){
                tipo = "conejo"
            }

            val vacuna1 = if (checkDistemper.isChecked) "check" else "noCheck"
            val vacuna2 = if (checkParvovirus.isChecked) "check" else "noCheck"
            val vacuna3 = if (checkHepatitis.isChecked) "check" else "noCheck"

            val bundle = Bundle().apply {
                putString("key_nombre",nombre)
                putString("key_edad",edad)
                putString("key_tipo",tipo)
                putString("key_vacuna1",vacuna1)
                putString("key_vacuna2",vacuna2)
                putString("key_vacuna3",vacuna3)
            }

            val intent = Intent(this,DestinoActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)



        }
    }
}